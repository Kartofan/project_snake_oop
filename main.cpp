#include <iostream>
#include <stdlib.h>
#include <cstdio>
#include <ctime>
#include <conio.h>
#include <windows.h>

using namespace std;

const int width = 20, height = 20;
int x, y, fruitX, fruitY, score = 0, nTail, difficulty;
int tailX[120], tailY[120];
char inputNum;
enum eDirection { STOP = 0, LEFT, RIGHT, UP, DOWN };
eDirection dir;

void Setup() {
    srand(time(0));
    dir = STOP;
    x = width / 2 - 1;
    y = height / 2 - 1;
    fruitX = rand() % (width - 1);
    fruitY = rand() % (height - 1);
    score = 0;
}
void Clear(){
    //system("clear");    //clear screen for linux
    system("cls");      //clear screen for windows
}

void Game_Over() {
    cout << "Game Over!" << endl;
    exit(0);
}

void Menu() {
    cout << "1. Play" << endl;
    cout << "2. Instruction" << endl;
    cout << "3. Exit" << endl;
    inputNum = _getch();
    Clear();
    switch (inputNum) {
    case '1':
                Clear();
                cout<<" Choose difficulty: "<<endl;
                cout<<"  Easy      - 1"<<endl;
                cout<<" Medium     - 2"<<endl;
                cout<<"  Hard      - 3"<<endl;
                cout<<" Your choice: ";
                difficulty = _getch();
                break;
    case '2':
        cout << "The goal of this game is to direct the Snake(using: w-up, s-down, d-right, a-right) on fruits(F letter) and after eating one Snake's body grow. ";
        cout << "When Snake hit border of the map or it's own body game ends. Do your best and eat as much fruits as u can!" << endl << endl;
        cout << "1. Back" << endl;
        cout << "2. Exit" << endl;
        inputNum = _getch();
        Clear();
        switch (inputNum)
        {
        case '1': Menu();
        case '2': Game_Over();
        }
    case '3': Clear();  Game_Over();
    }

}


void Draw() {
    bool allreadyspaced = false;
    Clear();

    cout << " ";
    for (int i = 0; i < width + 1; i++) {
        cout << "--";
    }

    cout << endl;

    for (int i = 0; i < height - 1; i++) {
        for (int j = 0; j < width; j++) {

            if (j == 0 || j == width - 1) {
                cout << " | ";
            }
            if (i == y && j == x) {
                if (dir == RIGHT || dir == 0) {
                    cout << "> ";
                }
                if (dir == LEFT) {
                    cout << "< ";
                }
                if (dir == DOWN) {
                    cout << "v ";
                }
                if (dir == UP) {
                    cout << "^ ";
                }

            }
            else if (i == fruitY && j == fruitX) {
                cout << "F ";
            }
            else {
                allreadyspaced = false;
                for (int k = 0; k < nTail; k++) {
                    if (tailX[k] == j && tailY[k] == i) {
                        cout << "* ";
                        allreadyspaced = true;
                    }

                }
                if (!allreadyspaced) {
                    cout << "  ";
                }
            }
        }
        cout << endl;
    }
    cout << " ";
    for (int i = 0; i < width + 1; i++) {
        cout << "--";
    }
    cout << endl << "score = " << score << endl;


    return;
}
void Input() {
    switch(difficulty){
        case '1' : Sleep(300); break;
        case '2' : Sleep(200); break;
        case '3' : Sleep(100); break;
    }

    if(kbhit())
    {

        inputNum = _getch();
        switch (inputNum) {
        case 'a': dir = LEFT; break;
        case 'd': dir = RIGHT; break;
        case 'w': dir = UP; break;
        case 's': dir = DOWN; break;
        case 'x': Game_Over();
        }
    }
}
void Logic() {
    int prevX = tailX[0];
    int prevY = tailY[0];
    int prev2X, prev2Y;
    tailX[0] = x;
    tailY[0] = y;
    for (int i = 1; i < nTail; i++) {
        prev2X = tailX[i];
        prev2Y = tailY[i];
        tailX[i] = prevX;
        tailY[i] = prevY;
        prevX = prev2X;
        prevY = prev2Y;
        if (x == tailX[i] && y == tailY[i]) {
            Game_Over();
        }
    }


    switch (dir) {
    case LEFT: x--; break;
    case RIGHT: x++; break;
    case UP: y--; break;
    case DOWN: y++; break;
    }

    if (x > width - 2 || x<0 || y>height - 2 || y < 0) {
        Game_Over();
    }
    if (x == fruitX && y == fruitY) {
        score++;
        nTail++;
        fruitX = rand() % (width - 1);
        fruitY = rand() % (height - 1);
    }
}



int main() {
    Menu();
    Setup();
    for (;;) {
        Draw();
        Input();
        Logic();
    }
}
